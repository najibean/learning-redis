- masuk ke dalam redis
```
docker container exec -i -t redis /bin/sh
# redis-cli -h localhost
```

- secara default total database adalah 16 (range 0 - 15)\
    untuk pindah database maka kita menggunakan `select <no_index>`, jika tidak ada no_index berarti pada posisi 0.

- set - get\
    ```
        set nama1 "fuad najibullah"
        set nama2 "arifa rasyadah ramadhani"
        set nama3 "sulaiman harits abdurrahman"
    ```
    _jika dilakukan get :_
    ```
        get nama1
    ``` 

- mengecek ada data atau tidak\
    menggunakan command `exists <nama_key>`, sama seperti boolean namun hasil yang dikeluarkan adalah 0 dan 1 (bukan true dan false).

- menghapus data\
    menggunakan `del <nama_key>`. Bisa juga lebih dari satu key = `del key1 key2 key3`

- menambah value data\
    menggunakan `append`\
    jadi jika ada value "fuad", kemudian dilakukan `append " najibullah"` maka ketika di-get yang key-nya hasilnya adalah "fuad najibullah".\
    namun _append_ juga bisa digunakan untuk insert data (selain _set_)

- mengetahui list key dengan pattern\
    misalkan command begini = `keys nama*` --> untuk mengetahui key nama1, nama2, nama3, dan seterusnya.
    ```
        localhost:6379> keys nam*
        1) "nama2"
        2) "nama4"
        3) "nama1"
        4) "nama3"
    ```

- mengambil value string untuk beberapa karakter aja\
    bisa dengan `getrange <nama_key> <index_awal> <index_akhir>`\
    contoh :
    ```
        localhost:6379> getrange nama1 0 2
        "naj"   
    ```

- operasi untuk multiple get\
    ```
        localhost:6379> mget nama1 nama2 nama3
        1) "najib"
        2) "raihan"
        3) "arifa"
    ```

### expiration

untuk membuat data yang sudah ada menjadi expire = `expire <nama_key> seconds`\
untuk membuat expired data dari awal = `setex <nama_key> seconds <value>`\
kemudian untuk mengetahui ttl (time to limit) / berapa lama waktu hidup dari sebuah key adalah `ttl <nama_key>`

### Increment & Decrement

- incr dan decr yang tidak butuh ada data terlebih dahulu\
    ```
        localhost:6379> incr count
        (integer) 1
        localhost:6379> get count
        "1"
        localhost:6379> incr count
        (integer) 2
        localhost:6379> incr count
        (integer) 3
        localhost:6379> get count
        "3"
        localhost:6379> decr count
        (integer) 2
        localhost:6379> get count
        "2"
    ```

- incr dan decr yang di set dahulu di awal, dan kali ini di set tipe nya int\
    ```
        localhost:6379> set numb 2
        OK
        localhost:6379> incr numb
        (integer) 3
        localhost:6379> get numb
        "3"
        localhost:6379> incrby numb 10
        (integer) 13
        localhost:6379> get numb
        "13"
    ```

### Flush

untuk men-delete secara menyeluruh data didalam database.\
mendelete di current database = `flushdb`\
mendelete di keseluruhan database = `flushall`\
contoh :
    ```
        localhost:6379[1]> flushdb
        OK
        localhost:6379[1]> keys *
        (empty array)
        localhost:6379[1]> select 0
        OK
        localhost:6379> keys *
        1) "nama2"
        2) "count"
        3) "nama3"
        4) "nama1"
        5) "nama4"
        6) "numb"
        localhost:6379> select 1
        OK
        localhost:6379[1]> flushall
        OK
        localhost:6379[1]> select 0
        OK
        localhost:6379> keys *
        (empty array)
    ```

### Pipeline

untuk membuat data yang besar sekaligus dan tidak mendapatkan reply/response dari redis satu-satu, solusinya adalah menggunakan pipeline.\
masuk ke dir data kita = `/usr/local/etc/<nama_dir>`\
misalkan nama file .txt adalah `sets.txt`\
selanjutnya dibuat begini = `cat sets.txt | redis-cli -h localhost --pipe`\
akan mendapatkan response :
```
    # cat sets.txt | redis-cli -h localhost --pipe
    All data transferred. Waiting for the last reply...
    Last reply received from server.
    errors: 0, replies: 1000
```
